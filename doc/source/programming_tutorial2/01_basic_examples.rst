.. include:: external_links.rst

.. _ProgTT_basics

Basics GUI programming
======================


.. tip::

    All examples are available as unit-test in ``programming_tutorial2/01_Basics/basic_gui_examples.py``.
    For example, to run the first example, right-mouse click into the code definition of ``test_example1_hello_world(self)``
    and call `Debug 'unit_test for BasicExamples.test_example1_hello_world'`.

    .. image:: img/pycharm_start_unit_test.png

.. _ProgTT_basics_exercise1_hello_world:

1. Hello World
--------------

Creating a Qt based GUI application is rather uncomplicated. As we start from scratch, we first need to
create a QApplication_ instance::

    from PyQt5.QtWidgets import QApplication, QWidget

    app = QApplication([])

Calling QApplication_ initializes the Qt framework, enabling it to draw widgets on your screen and to react on user
inputs. If you like to start a GUI from the QGIS python shell or
an EnMAP-Box instance, this has been already done for your. If we want to create GUI from our own python
shell, e.g. as started from the PyCharm debug mode, we need to start the QApplication by our own.

.. note::
    QApplication_ needs to be instantiated *only once*, or as the `Qt documentation <https://doc.qt.io/qt-5/qapplication.html#details>`_ writes:

    `For any GUI application using Qt, there is precisely one QApplication_ object, no matter whether the application has
    0, 1, 2 or more windows at any given time.
    For non-QWidget based Qt applications, use QGuiApplication instead, as it does not depend on the QtWidgets library.`

Now we can create our first widget, for which we create a QWidget_::

    widget = QWidget()
    widget.setWindowTitle('Hello World')

The widget is still hidden. We make it visible calling::

    widget.show()


.. tip::

    QWidget_ is the base class of all other Qt widgets. It offers plenty of customizable options, e.g.::

        widget.setFixedSize(150, 200) # change width and height
        widget.setVisible(False) # hide the widget


Finally we need to enter the main applications event loop to allow user interactions. If we don't do, the widget
would show up for some milliseconds and disappear immediately, as the main application is finished and terminates::

    app.exec_()


.. figure:: img/example1_hello_world.png
    :width: 50%

    Example 1: An empty *Hello World* widget.



**Summary**

You now leaned how to:

* initialize a QApplication_
* create QWidgets_ and show them on screen

**Complete code**

.. code-block:: python

    from PyQt5.QtWidgets import QApplication, QWidget

    app = QApplication([])

    widget = QWidget()
    widget.setWindowTitle('Hello World')
    widget.show()

    app.exec_()





2. Hello Real World
-------------------

This tutorial and the EnMAP-Box is for remote sensing people, so let's visualize *real world* spatial data with the
`QGIS API <https://qgis.org/api/>`_. Similar to the Qt framework and its QApplication_, we first need to initialize the
QGIS API by creating a QgsApplication_. To simplify a couple of other issues, wie use the EnMAP-Box API for doing this::

    from enmapbox.testing import initQgisApplication
    app = initQgisApplication()

.. note::

    * QgsApplication_ inherits * QApplication_ *, so it's not required
      to create a separated * QApplication_ *

    * If you use the QGIS API from a QGIS python shell or the EnMAP-Box, the QgsApplication has already been done for you.

    * ``enmapbox.testing.initQgisApplication(...)`` creates a QgsApplication_ and emulates a QGIS Desktop Application, e.g.
      by loading different raster drivers and initializing the QGIS Processing Framework.


Now we can create a QgsRasterLayer_ that shows the world, as presented in the google satellite map::

    from qgis.core import QgsRasterLayer
    uri = r'crs=EPSG:3857&format&type=xyz&url=https://mt1.google.com/vt/lyrs%3Ds%26x%3D%7Bx%7D%26y%3D%7By%7D%26z%3D%7Bz%7D&zmax=19&zmin=0'
    layer = QgsRasterLayer(uri, 'google maps', 'wms')
    assert layer.isValid()




A QgsMapCanvas_ is used to visualize QgsMapLayers_, like our QgsRasterLayer_. Before it can be rendered on a QgsMapCanvas_,
the layer needs to be stored in a QgsMapLayerStore_, e.g those maintained in the current QgsProject_ instance::

        QgsProject.instance().addMapLayer(layer)

        from qgis.gui import QgsMapCanvas
        canvas = QgsMapCanvas()
        canvas.setWindowTitle('Hello Real World')
        canvas.setLayers([layer])

To focus the map canvas on our raster
layer and its spatial extent, we just need to call::

        canvas.setExtent(layer.extent())
        canvas.setDestinationCrs(layer.crs())

Finally, we show the canvas widget and start the main application loop::

        canvas.show()
        app.exec_()


.. figure:: img/example2_hello_real_world.png
    :width: 50%

    Example 2: QgsMapCanvas_ visualizing the google satellite map.


In most cases you probably like to show a local file-based image, like that in the EnMAP-Box testdata::

    import enmapboxtestdata
    uri = enmapboxtestdata.enmap
    layer = QgsRasterLayer(uri)

.. figure:: img/example2_hello_real_world_berlin.png
    :width: 50%

    Example 2b: QgsMapCanvas_ visualizing the EnMAP example image.


The canvas CRS can be set to any CRS of choice. This becomes impressive when projecting the global google satellite map
as UTM for Zone 32N(`EPSG code 32632 <http://spatialreference.org/ref/epsg/32632/>`_)::

    from qgis.core import QgsCoordinateReferenceSystem
    canvas.setDestination(QgsCoordinateReferenceSystem('EPSG:32632'))


.. figure:: img/example2_hello_real_world_utm32n.png
    :width: 50%

    Example 2c: The google satellite map projected in UTM Zone 32 N (EPSG:32632).


**Summary**

You now can:

* open raster data sources as QgsRasterLayer_

* visualize QgsMapLayers_ on a QgsMapCanvas_

* create a QgsCoordinateReferenceSystem_ for any EPSG code of interest

* change the map canvas CRS

**Full code**

.. code-block:: python

        from enmapbox.testing import initQgisApplication

        app = initQgisApplication()

        assert isinstance(app, QApplication)
        assert isinstance(app, QgsApplication)

        uri = r'crs=EPSG:3857&format&type=xyz&url=https://mt1.google.com/vt/lyrs%3Ds%26x%3D%7Bx%7D%26y%3D%7By%7D%26z%3D%7Bz%7D&zmax=19&zmin=0'
        layer = QgsRasterLayer(uri, 'google maps', 'wms')

        import enmapboxtestdata
        layer = QgsRasterLayer(enmapboxtestdata.enmap)

        QgsProject.instance().addMapLayer(layer)

        assert layer.isValid()


        from qgis.gui import QgsMapCanvas
        canvas = QgsMapCanvas()
        canvas.setWindowTitle('Hello Real World')
        canvas.setLayers([layer])
        canvas.setExtent(layer.extent())
        canvas.setDestinationCrs(layer.crs())
        canvas.show()

        app.exec_()



.. important::

    This tutorial use several objects from the libraries QtCore_, QtGui_, QtWidget_, qgis.core_ and qgis.gui_.
    Object names from these libraries do not interfere, so we will import them for simplicity once
    in the beginning of our python project::

        from qgis.PyQt.QtCore import *
        from qgis.PyQt.QtGui import *
        from qgis.PyQt.QtWidgets import *
        from qgis.core import *
        from qgis.gui import *






3. Create own widgets (I)
-------------------------

`Widgets` can be simple, like a singles buttons, or more complex, like a formula to enter different types of input.
Widgets are created by inheriting from a standard widget and enhancing it with our widgets and background logic.

Let's create a simple form that has:

 * a map to show spatial data

 * a text box to show multi-line textual data

 * a label to show other information

 * a button the user can click to reset the map

First, we create a new class that inherits from ``QWidget`::

    class ExampleWidget(QWidget):

    def __init__(self, parent=None):
        super(ExampleWidget, self).__init__(parent)
        self.setWindowTitle('Example Widget')
        self.resize(QSize(300,200))

``super(ExampleWidget, self).__init__(parent)`` calls the constructor of ``QWidget`` and initializes all properties
and functions available for QWidgets_. After that we can add the other widgets::

        self.textBox = QTextEdit()
        self.mapCanvas = QgsMapCanvas()
        self.label = QLabel('Label info')
        self.button = QPushButton('Press me')

To define their position, we use two nested layouts. In general we like to order the widgets in rows of a major vertical
QVBoxLayout_. To this we add two rows with our widget. Each row is realized with a QHBoxLayout_ ::


        self.setLayout(QVBoxLayout())

        self.topLayout = QHBoxLayout()
        self.topLayout.addWidget(self.textBox)
        self.topLayout.addWidget(self.mapCanvas)

        self.bottomLayout = QHBoxLayout()
        self.bottomLayout.addWidget(self.label)
        self.bottomLayout.addWidget(self.button)

        self.layout().addLayout(self.topLayout)
        self.layout().addLayout(self.bottomLayout)


We use the map canvas to show a raster image::

        from enmapboxtestdata import enmap
        layer = QgsRasterLayer(enmap)
        QgsProject.instance().addMapLayer(layer)
        self.mapCanvas.setLayers([layer])
        self.mapCanvas.setDestinationCrs(layer.crs())
        self.mapCanvas.setExtent(self.mapCanvas.fullExtent())


Let's start and visualize our widget::

        app = initQgisApplication()

        myWidget = ExampleWidget()
        myWidget.show()

        app.exec_()


.. image:: img/example3a_basic.png

Summary
~~~~~~~

**You can now**

* create your own widget

* order widgets in vertical, horizontal and nested layouts


**Full code example**

`ExampleWidget` definition

.. code-block:: python


    class ExampleWidget(QWidget):

        def __init__(self, parent=None):
            super(ExampleWidget, self).__init__(parent)

            self.setWindowTitle('Example Widget')
            self.resize(QSize(300, 200))

            self.textBox = QTextEdit()
            self.mapCanvas = QgsMapCanvas()
            self.label = QLabel('Label info')
            self.button = QPushButton('Press me')

            self.setLayout(QVBoxLayout())

            self.topLayout = QHBoxLayout()
            self.topLayout.addWidget(self.textBox)
            self.topLayout.addWidget(self.mapCanvas)

            self.bottomLayout = QHBoxLayout()
            self.bottomLayout.addWidget(self.label)
            self.bottomLayout.addWidget(self.button)

            self.layout().addLayout(self.topLayout)
            self.layout().addLayout(self.bottomLayout)

            from enmapboxtestdata import enmap
            layer = QgsRasterLayer(enmap)
            QgsProject.instance().addMapLayer(layer)
            self.mapCanvas.setLayers([layer])
            self.mapCanvas.setDestinationCrs(layer.crs())
            self.mapCanvas.setExtent(self.mapCanvas.fullExtent())

Calling code

.. code-block:: python

        app = initQgisApplication()

        myWidget = ExampleWidget()
        myWidget.show()

        app.exec_()



4. Signals and Slots (I)
------------------------


Graphical User Interfaces react on various types of user interactions and inputs. For example

* a click on a `Start` button that starts a time-demanding process

* a click on a `Cancel` button that interrupts a time-demanding process

* a tooltip that is shown when the mouse cursor stops over a widget

* a text box that changes its background color into red when its input becomes invalid

With Qt such interactions are often realized with `Signals and Slots <target="_blank" https://doc.qt.io/qt-5/signalsandslots.html>`_ .
All widgets can emit signals to show that their state has changed. Signals can be connected to
functions to react on state changes. In Qt terminology, these functions are called *slots*.

Let's enhance our `ExampleWidget` to react on the button's `.clicked() <https://doc.qt.io/qt-5/qabstractbutton.html#clicked>`_
signal. We define the slot function `onButtonClicked` to add some text to the text box and update the label::

    def onButtonClicked(self, *args):
        text = self.textBox.toPlainText()
        text = text + '\nclicked'
        self.textBox.setText(text)

        from datetime import datetime
        self.label.setText('Last click {}'.format(datetime.now().time()))

Then we modify the *ExampleWidget's* constructor (``__init__``) and connect the `clicked` signal with the `onButtonClicked` slot::

    self.button.clicked.connect(self.onButtonClicked)

In addition we like to reset the map canvas to the full layer extent, e.g. after a user has change the spatial extent
by zooming or panning. We like to keep our source-code clear and short. The reset operation can be defined in
one-line, so we use a `lambda function <https://www.w3schools.com/python/python_lambda.asp>` and connect with the button click::

    self.button.clicked.connect(lambda : self.mapCanvas.setExtent(self.mapCanvas.fullExtent()))

Re-start the widget, zoom or pan within the map canvas and click the button:

.. image:: img/example4_signals_examplewidget_pressme.png


**Summary**

You now can:

* use signals and connect them to slots

* define lambda functions and use them as slot


5. Qt Designer
--------------

The more widgets your GUI frontend contains, the more you have to specify details like widget positions, layout behaviour,
button names etc. This easily becomes a hassle if every widget position and size etc. has to be programmed manually.
Instead, tools like the Qt Designer and Qt Creator allow you to design a GUI *frontend* by drag 'n drop.
This helps focusing on visual aspects and separates front-end from background logic.

Start the Qt Designer (or Qt Creator), click on `File` > `New..` and create a new ``exampleform.ui`` that is based on *Widget*.

.. figure:: img/example5_new_form_widget.png

    Qt Designer Dialog to create a new form file (``*.ui``)


.. image:: example5_new_form_widget1.png

    Qt Designer showing the empty ``exampleform.ui``


Now take *at least* 10 minutes to explore the Qt Designer:

* Drag and drop widgets from `Widget Box` to your empty form

* Add layouts from the `Widget Box` to your form and add widgets into

* Explore and modify widget properties with the `Property Editor`

* visualize previews via `View` > `Preview` (or CTRL + P)


.. image:: img/qt_designer_widget_box.png
    :width: 30%

.. image:: img/qt_designer_property_editor.png
    :width: 30%

.. image:: img/qt_designer_object_inspector.png
    :width: 30%


Save your changed `exampleform.ui` (CTRL+S) and inspect it in a text editor. As you see, the Qt Designer describes the
entire frontend design in an XML structure. This can be read from python to define a widget class for which we just need to
write the backend::


    pathUi = os.path.join(os.path.dirname(__file__), 'exampleform.ui')
    from enmapbox.gui.utils import loadUIFormClass
    class ExampleUI(QWidget, loadUIFormClass(pathUi)):

        def __init__(self, parent=None):
            super(ExampleUI, self).__init__(parent)
            self.setupUi(self)


Now open our widget from python::

        app = initQgisApplication()

        w = ExampleUI()
        w.show()

        app.exec_()


.. tip::

    ``enmapbox.gui.utils import loadUIFormClass`` loads a `*.ui` file and returns its form class. It enhances
    ```uic.loadUiType(...)`` <http://pyqt.sourceforge.net/Docs/PyQt5/designer.html#pyuic5>`_ by considering several issues
    related to QGIS libraries.



Now continue using the Qt Designer and copy the design given in ``programming_tutorial2/01_Basics/exampleform.ui``.

* a horizontal layout that contains a label, QPushButtons_ and a QgsMapLayerComboBox_

* a QGraphicsView_. It will become a QgsMapCanvas in the next exercise, here we need it as placeholder

* a QgsCollapsibleGroupBox_ with a label and a QgsColorButton_

* test your design with the form preview. check how widgets behave to resize operations

.. figure:: img/example5_exampleform.png
    :width: 75%

    ``programming_tutorial2/01_Basics/exampleform.ui`` as seen in Qt Designer


6. Promote widgets
------------------

Many widgete are not defined in the Qt Designer's widget list.


ADVANCED 5. Implement virtual functions
---------------------------------

Not each state-change will trigger a signal, but some become available as `virtual function <https://en.wikipedia.org/wiki/Virtual_function>`.
In that case we can re-implement these function to react on changes. For example we like to know the mouse cursors position when it hovers over the text box.

The text box is a QTextEdit_ widget, which implements QAbstractScrollArea_ and defines a virtual `mouseMoveEvent <https://doc.qt.io/qt-5/qabstractscrollarea.html#mouseMoveEvent>`_:

.. code-block:: batch

    [override virtual protected] void QAbstractScrollArea::mouseMoveEvent(QMouseEvent *e)
    Reimplemented from QWidget::mouseMoveEvent().
    This event handler can be reimplemented in a subclass to receive mouse move events for the viewport() widget. The event is passed in e.
    See also QWidget::mouseMoveEvent().


We first create a new function with same structure as the virual `mouseMoveEvent` to update the label text::

    def textEditMouseMoveEvent(self, mouseEvent: QMouseEvent):

        info = 'Mouse position {},{}'.format(mouseEvent.x(), mouseEvent.y())
        self.label.setText(info)

Then we modify `ExampleWidgets.__init__` and overwrite the old `mouseMoveEvent` with its new definition::

    self.textBox.mouseMoveEvent = self.textEditMouseMoveEvent

Restart the widget and hover the mouse cursor over the text box to update the label text with the mouse coordinates.

.. image:: example5_implement_virtual_functions.png


We like to do the same for the map canvas and show it's cursor's location as well. In particular we want to show the
coordinate values not as pixel coordinate but CRS coordinates. In this case we need to be aware that the
`mouseMoveEvent <https://qgis.org/api/classQgsMapCanvas.html#aab97a435d2276ed8ef8d7cdc50fdcddc>`_ is
already implemented and *not virtual any more*.

Therefore we create our own map canvas as followed:


QgsMapCanvas.



5. Create Context Menus
-----------------------

6. Define new signals
---------------------











X. Template
-------------------

Summary


*Full code*

.. code-block:: python

        template

Now you learned
...............

* template

* template



